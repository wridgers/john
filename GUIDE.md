# Style guide for john

A quick style guide for john. If you plan on submitting a pull request, please
follow these guidelines.

  - As much commenting as possible
    - Every line commented
    - Include links to techniques you have used
  - No single letter variables
