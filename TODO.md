# A list of things to do

  - John
    * command line options
    * log file
    * use library to output image
    * use steganography techniques to store render details in image
    * ray tree debugging
      - ability to output entire ray tree for all pixels (warning, large file)
        and the visualise this (WebGL viewer?) to debug casting
    * tests
    * doxygen

  - Core
    * scene handler
    * load scene from file
      - blender exporter
    * binary space partitioning
    * time factor for animation and motion blur

  - Objects
    * triangles
    * boxes
    * cylinder

  - Quality
    * anti aliasing
      - grid, jitter, adaptive

  - Lighting
    * soft shadows
    * global illumination
    * spot lights
    * volume lights
    * track energy of ray, disregard once energy drops below threshold

  - Camera
    * support for different lenses
    * aperture shape
    * depth of field

  - Materials
    * refraction
    * textures
    * bump mapping
    * checkerboard patterns
    * named materials
    * preconfigured set of common materials


